package com.nedioit.employe.service;

import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.nedioit.employe.dto.EmployeRequest;
import com.nedioit.employe.dto.EmployeResponse;
import com.nedioit.employe.entities.Employe;
import com.nedioit.employe.mapper.EmployeMapper;
import com.nedioit.employe.repository.EmployeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeServiceImpl implements IEmployeService {

    Logger logger=LoggerFactory.getLogger(EmployeServiceImpl.class);
    String employeNotFond = "Employe not found";

    private final EmployeRepository employeRepository;
    private final EmployeMapper employeMapper;

    public EmployeServiceImpl(EmployeRepository employeRepository, EmployeMapper employeMapper) {
        this.employeRepository = employeRepository;
        this.employeMapper = employeMapper;
    }

    @Override
    public EmployeResponse add(EmployeRequest employeRequest) {
        try {
            Employe employe = employeMapper.fromEmployeRequest(employeRequest);
            employeRepository.save(employe);
            logger.info("Employe added: {}", employe);
            return employeMapper.fromEmploye(employe);
        } catch (Exception ex) {
            logger.error("Error while adding Employe", ex);
            throw ex;
        }
    }

    @Override
    public EmployeResponse getEmployeById(Long id) {
        try {
            Optional<Employe> optionalEmploye = employeRepository.findById(id);
            if (optionalEmploye.isPresent()) {
                Employe employe = optionalEmploye.get();
                logger.info("Employe found by id {}: {}", id, employe);
                return employeMapper.fromEmploye(employe);
            } else {
                throw new EntityNotFoundException(employeNotFond + id + " not found");
            }
        } catch (Exception ex) {
            logger.error("Error while getting Employe by id {}", id, ex);
            throw ex;
        }
    }

    @Override
    public List<EmployeResponse> getAllEmployes() {
        try {
            Sort sortById = Sort.by(Sort.Direction.DESC, "id");
            List<Employe> employes = employeRepository.findAll(sortById);
            List<EmployeResponse> employeRespons = new ArrayList<>();
            for (Employe employe : employes) {
                employeRespons.add(employeMapper.fromEmploye(employe));
            }
            logger.info("Retrieved {} employes", employes.size());
            return employeRespons;
        } catch (Exception ex) {
            logger.error("Error while getting all Employes", ex);
            throw ex;
        }
    }

    @Override
    public EmployeResponse updateEmploye(Long id, EmployeRequest employeRequest) {
        try {
            Optional<Employe> optionalProduct = employeRepository.findById(id);
            if (optionalProduct.isPresent()) {
                Employe employe = optionalProduct.get();
                employe.setNom(employeRequest.getNom());
                employe.setPrenom(employeRequest.getPrenom());
                employe.setPoste(employeRequest.getPoste());
                employe.setSalaire(employeRequest.getSalaire());
                employeRepository.save(employe);
                logger.info("Employe updated: {}", employe);
                return employeMapper.fromEmploye(employe);
            } else {
                throw new EntityNotFoundException(employeNotFond + id + " not found");
            }
        } catch (Exception ex) {
            logger.error("Error while updating Employe with id {}", id, ex);
            throw ex;
        }
    }

    @Override
    public void deleteEmploye(Long id) {
        try {
            Optional<Employe> optionalProduct = employeRepository.findById(id);
            if (optionalProduct.isPresent()) {
                Employe employe = optionalProduct.get();
                employeRepository.delete(employe);
                logger.info("Employe deleted: {}", employe);
            } else {
                throw new EntityNotFoundException(employeNotFond + id + " not found");
            }
        } catch (Exception ex) {
            logger.error("Error while deleting Employe with id {}", id, ex);
            throw ex;
        }
    }
}