package com.nedioit.employe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeApplication {

	public static void main(String[] args) {
		//ElasticApmAttacher.attach();
		SpringApplication.run(EmployeApplication.class, args);
	}

}
