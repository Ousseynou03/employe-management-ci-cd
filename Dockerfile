FROM eclipse-temurin:17-jdk-alpine
VOLUME  /tmp
ARG JAR_FILE=target/*.jar
COPY    ./target/employe-0.0.1-SNAPSHOT.jar  employe.jar
ENTRYPOINT ["java", "-jar", "employe.jar"]
